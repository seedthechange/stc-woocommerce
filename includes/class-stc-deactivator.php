<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://bryceyork.com
 * @since      1.0.0
 *
 * @package    Stc
 * @subpackage Stc/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Stc
 * @subpackage Stc/includes
 * @author     Bryce York <bryce@seedthechange.org>
 */
class Stc_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
