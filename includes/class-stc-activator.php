<?php

/**
 * Fired during plugin activation
 *
 * @link       http://bryceyork.com
 * @since      1.0.0
 *
 * @package    Stc
 * @subpackage Stc/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Stc
 * @subpackage Stc/includes
 * @author     Bryce York <bryce@seedthechange.org>
 */
class Stc_Activator {

	/**
	 * Set logical defaults.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		update_option( 'stc_settings', [
			'stc_vendorName' => mb_convert_encoding(get_bloginfo( 'name', 'display' ), 'UTF-8', 'HTML-ENTITIES'),
			'stc_showCounter' => '1'
		] );
	}

}
