<?php

    /**
     * The public-facing functionality of the plugin.
     *
     * @link       http://bryceyork.com
     * @since      1.0.0
     *
     * @package    Stc
     * @subpackage Stc/public
     */

    /**
     * The public-facing functionality of the plugin.
     *
     * Defines the plugin name, version, and two examples hooks for how to
     * enqueue the admin-specific stylesheet and JavaScript.
     *
     * @package    Stc
     * @subpackage Stc/public
     * @author     Bryce York <bryce@seedthechange.org>
     */
    class Stc_Public {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        private $version;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         *
         * @param      string $plugin_name The name of the plugin.
         * @param      string $version The version of this plugin.
         */
        public function __construct ($plugin_name, $version) {
            $this->plugin_name = $plugin_name;
            $this->version     = $version;
        }

        /**
         * Register the stylesheets for the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function enqueue_styles () {

            /**
             * This function is provided for demonstration purposes only.
             *
             * An instance of this class should be passed to the run() function
             * defined in Stc_Loader as all of the hooks are defined
             * in that particular class.
             *
             * The Stc_Loader will then create the relationship
             * between the defined hooks and the functions defined in this
             * class.
             */

            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/stc-public.css', [], $this->version, 'all');
        }

        /**
         * Register the JavaScript for the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function enqueue_scripts () {

            /**
             * This function is provided for demonstration purposes only.
             *
             * An instance of this class should be passed to the run() function
             * defined in Stc_Loader as all of the hooks are defined
             * in that particular class.
             *
             * The Stc_Loader will then create the relationship
             * between the defined hooks and the functions defined in this
             * class.
             */

            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/stc-public.js', ['jquery'], $this->version, false);
        }

	    public function inject_stc_trigger() {
		    $stc_options = get_option( 'stc_settings' );

            if ($stc_options['stc_showCounter'] == true) {
		        echo sprintf( "<script>window.autc('%s','%s',%s)</script>", addslashes($stc_options['stc_vendorId']), addslashes($stc_options['stc_vendorName']), $stc_options['stc_debugMode']);
            }
	    }

	    /**
         * Register the webhook.
         *
         * @since    1.0.0
         */
        public function register_stc_webhook ($order_id) {
            syslog(LOG_INFO, "Syslog: Running register_stc_webhook...");
            $order    = wc_get_order($order_id);
            $products = $order->get_items();
            $items    = [];

            foreach ($products as $prod) {
                $items[] = [
                    'product_id'      => $prod['product_id'],
                    'variation_id'    => $prod['variation_id'],
                    'product_name'    => $prod['name'],
                    'product_qty'     => $prod['qty'],
                    'line_total_cost' => $prod['line_total'],
                ];
            }

            $stc_options     = get_option('stc_settings');
            $stc_root_domain = ($stc_options['stc_debugMode'] == 1) ? 'http://staging-seedthechange.herokuapp.com' : 'https://api.seedthechange.org';
            $url             = $stc_root_domain . '/v2/woocommerce/plant/' . $stc_options['stc_vendorId'] . '/';
            $payload         = [
                'order_id'      => $order->order_key,
                'first_name'    => $order->billing_first_name,
                'last_name'     => $order->billing_last_name,
                'billing_email' => $order->billing_email,
                'order_items'   => $items,
                'order_meta'    => [
                    'order_timestamp'  => $order->order_date,
                    'ip_address'       => $order->customer_ip_address,
                    'total'            => $order->order_total,
                    'payment_method'   => $order->payment_method,
                    'currency'         => $order->order_currency,
                    'shipping_address' => [
                        'address_1' => $order->shipping_address_1,
                        'address_2' => $order->shipping_address_2,
                        'city'      => $order->shipping_city,
                        'state'     => $order->shipping_state,
                        'postcode'  => $order->shipping_postcode,
                        'country'   => $order->shipping_country,
                    ],
                ],
            ];

            // post to the request somehow
            wp_remote_post($url, [
                'method'      => 'POST',
                'timeout'     => 45,
                'redirection' => 5,
                'httpversion' => '1.0',
                'blocking'    => true,
                'headers'     => [
                    'Content-Type' => 'application/json',
                ],
                'body'        => json_encode($payload),
                'cookies'     => [],
            ]);
        }
    }
