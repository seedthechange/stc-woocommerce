<?php

    /**
     * The admin-specific functionality of the plugin.
     *
     * @link       http://bryceyork.com
     * @since      1.0.0
     *
     * @package    Stc
     * @subpackage Stc/admin
     */

    /**
     * The admin-specific functionality of the plugin.
     *
     * Defines the plugin name, version, and two examples hooks for how to
     * enqueue the admin-specific stylesheet and JavaScript.
     *
     * @package    Stc
     * @subpackage Stc/admin
     * @author     Bryce York <bryce@seedthechange.org>
     */
    class Stc_Admin {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        private $version;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         *
         * @param      string $plugin_name The name of this plugin.
         * @param      string $version The version of this plugin.
         */
        public function __construct ($plugin_name, $version) {
            $this->plugin_name = $plugin_name;
            $this->version     = $version;
        }

        /**
         * Display admin page
         *
         * @since   1.0.0
         */
        public function display_admin_page () {
            add_menu_page(
                'Seed The Change Admin', // $page_title
                'Seed The Change', // $menu_title
                'manage_options', // $capability
                'seed-the-change-admin', // $menu_slug
                [$this, 'showPage'], // $function
                '', // todo: $icon_url
                '3.0' // menu position
            );
        }

        /**
         * Helper method for display_admin_page
         *
         * @since  1.0.0
         */
        public function showPage () {
            include 'partials/stc-admin-display.php';
        }


        /**
         * Register the stylesheets for the admin area.
         *
         * @since    1.0.0
         */
        public function enqueue_styles () {

            /**
             * This function is provided for demonstration purposes only.
             *
             * An instance of this class should be passed to the run() function
             * defined in Stc_Loader as all of the hooks are defined
             * in that particular class.
             *
             * The Stc_Loader will then create the relationship
             * between the defined hooks and the functions defined in this
             * class.
             */

            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/stc-admin-twbs.css', [], $this->version, 'all');
            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/stc-admin.css', [], $this->version, 'all');
        }

        /**
         * Register the JavaScript for the admin area.
         *
         * @since    1.0.0
         */
        public function enqueue_scripts () {

            /**
             * This function is provided for demonstration purposes only.
             *
             * An instance of this class should be passed to the run() function
             * defined in Stc_Loader as all of the hooks are defined
             * in that particular class.
             *
             * The Stc_Loader will then create the relationship
             * between the defined hooks and the functions defined in this
             * class.
             */

            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/stc-admin.js', ['jquery'], $this->version, false);
        }

        /**
         * Register custom settings
         *
         * @since    1.0.0
         */
        public function stc_settings_init () {
            register_setting('pluginPage', 'stc_settings');

            add_settings_section(
                'stc_pluginPage_section',
                '',
                'stc_settings_section_callback',
                'pluginPage'
            );

            add_settings_field(
                'stc_vendorId',
                __('Vendor ID', 'st'),
                'stc_vendorId_render',
                'pluginPage',
                'stc_pluginPage_section'
            );

            add_settings_field(
                'stc_vendorName',
                __('Vendor Name', 'st'),
                'stc_vendorName_render',
                'pluginPage',
                'stc_pluginPage_section'
            );

            add_settings_field(
                'stc_showCounter',
                __('Show universal live counter', 'st'),
                'stc_showCounter_render',
                'pluginPage',
                'stc_pluginPage_section'
            );

            add_settings_field(
                'stc_debugMode',
                __('Enable debug mode', 'st'),
                'stc_debugMode_render',
                'pluginPage',
                'stc_pluginPage_section'
            );

            // todo: add settings field for specifying the tree planting program info page to be linked to
            // todo: add settings field (rich text) for "product page snippet" that is made available via shortcode

            function stc_vendorId_render () {
                $options = get_option('stc_settings');
                ?>
                <input type='text' class="regular-text" name='stc_settings[stc_vendorId]'
                       value='<?php echo $options['stc_vendorId']; ?>'>
                <?php
            }

            function stc_vendorName_render () {
                $options = get_option('stc_settings');
                ?>
                <input type='text' class="regular-text" name='stc_settings[stc_vendorName]'
                       value="<?php echo htmlspecialchars($options['stc_vendorName']); ?>">
                <p class="description">NB: some special character won't work here, please <a href="mailto:support@seedthechange.org" target="_blank" rel="noopener">email us</a> if this is an issue.</p>
                <?php
            }

            function stc_showCounter_render () {
                $options = get_option('stc_settings');
                ?>
                <input type='checkbox'
                       name='stc_settings[stc_showCounter]' <?php checked($options['stc_showCounter'], 1); ?>
                       value='1'>
                <p class="description">Should we add the live counter to the top of each page on your site?</p>
                <?php
            }

            function stc_debugMode_render () {
                $options = get_option('stc_settings');
                ?>
                <input type='checkbox'
                       name='stc_settings[stc_debugMode]' <?php checked($options['stc_debugMode'], 1); ?>
                       value='1'>
                <p class="description">WARNING: only enable if advised by STC staff, this will significantly slow down
                    your site.</p>
                <?php
            }

            function stc_settings_section_callback () {
                echo __('Please enter your vendor name as instructed in the welcome pack. If you are unsure, contact <a href="mailto:support@seedthechange.org">support@seedthechange.org</a>.', 'st');
            }
        }

        public function stc_add_admin_menu () {
            add_options_page('Seed The Change Settings', 'Seed The Change', 'manage_options', 'stc', 'stc_options_page');

            function stc_options_page () {
                ?>
                <form action='options.php' method='post'>
                    <h2>Seed The Change Settings</h2>
                    <?php
                        settings_fields('pluginPage');
                        do_settings_sections('pluginPage');
                        submit_button();
                    ?>
                </form>
                <?php
            }
        }

    }
