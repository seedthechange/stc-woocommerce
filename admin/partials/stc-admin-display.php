<?php

    /**
     * Provide a admin area view for the plugin
     *
     * This file is used to markup the admin-facing aspects of the plugin.
     *
     * @link       http://bryceyork.com
     * @since      1.0.0
     *
     * @package    Stc
     * @subpackage Stc/admin/partials
     */

    $stc_options     = get_option('stc_settings');
    $stc_root_domain = ($stc_options['stc_debugMode'] == 1) ? 'http://staging-seedthechange.herokuapp.com' : 'https://api.seedthechange.org';
    if ($stc_options['stc_debugMode'] == 1) {
        echo '<h2 style="color:darkred;text-align:center;">WARNING: Debug Mode is enabled!</h2>';
    }
?>

<style>#stc-admin .hndle {cursor: default !important;}</style>

<main class="wrap container-fluid" id="stc-admin">
    <h1>Seed The Change Admin</h1>

    <div class="metabox-holder row">
        <div class="col-md-6">
            <div class="postbox">
                <h2 class="hndle">Overview</h2>
                <div class="inside">
                    <p><strong>The Seed The Change plugin is currently running on your store.</strong></p>
                    <p>You've planted <span class="SeedTheChangeCount">#</span> trees. Congratulations!</p>
                    <script type="text/javascript">
                        jQuery(function () {
                            jQuery.getJSON('<?php echo $stc_root_domain; ?>/v2/get/<?php echo $stc_options['stc_vendorId']; ?>/?jsoncallback=?', function (data) {
                                var treecount = csn(data[0]);
                                console.log(treecount);
                                jQuery('span.SeedTheChangeCount').text(treecount);
                            });
                        })

                        function csn(val) {
                            while (/(\d+)(\d{3})/.test(val.toString())) {
                                val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                            }
                            return val;
                        }
                    </script>
                    <p>Each order placed by your customers is processed by our system and used to update your account's
                        tree counter.</p>
                    <p>Your customer will be sent an email from our planting verification system to let them know that
                        we are planting trees on their behalf. We will also confirm the number of trees planted on their
                        behalf.</p>
                    <p>Thanks for being an official Seed The Change partner!</p>
                    <p><br>Regards,<br><em>Seed The Change Management</em></p>
                    <p>P.S If you want to customize the look and feel of your live counter, we recommend using built-in Wordpress Customize (in the menu to the left Appearance > Customize) and on that page there's an option labelled 'Additional CSS'. The standard styles can be found below for you to customize as needed.</p>
                    <pre>html {
    padding-top: 35px;
}
.universal-tree-counter {
    z-index: 16777271;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    color: #264A49;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    text-align: center;
    background-color: #fff;
    padding: 8px 12px;
    font-size: 13px;
}
.SeedTheChangeCount {
    font-weight: 700;
}
.stc-loading-icon, .stc-tree-icon {
    display: inline-block;
    width: 22px;
    height: 22px;
    margin-bottom: -6px;
    background-size: 22px;
}
.stc-loading-icon {
    background-image: url(https://s3-us-west-2.amazonaws.com/seed-the-change/loading-22px%402x.gif);
}
.stc-tree-icon {
    background-image: url(https://s3-us-west-2.amazonaws.com/seed-the-change/tree-icon-tangaroa.svg);
}</></pre>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="postbox">
                <h2 class="hndle">Photos & Partner Banners
                    <small>- these images are free to use for all current STC Partners</small>
                </h2>
                <div class="inside">
                    <p>Download photos from the <a
                            href="https://seedthechange.org/wp-content/themes/wp-stc/src/images/welcome/photos-from-madagascar-project.zip"><strong>Madagascar
                                Project</strong></a> (12 photos).</p>
                    <p>Download photos from the <a
                            href="https://seedthechange.org/wp-content/themes/wp-stc/src/images/welcome/photos-from-ethiopia-project.zip"><strong>Ethiopia
                                Project</strong></a> (8 photos).</p>
                    <p>Download photos from the <a
                            href="https://seedthechange.org/wp-content/themes/wp-stc/src/images/welcome/photos-from-haiti-project.zip"><strong>Haiti
                                Project</strong></a> (5 photos).</p>
                    <p>Download <a
                            href="https://seedthechange.org/wp-content/themes/wp-stc/src/images/welcome/deforestation-impact.zip"><strong>deforestation
                                impact photos</strong></a> (7 photos).</p>
                    <hr>
                    <p>Download <strong><a
                                href="https://s3-us-west-2.amazonaws.com/seed-the-change/seed-the-change-banners-2017.zip"
                                target="_blank" rel="noopener">Partner Banners</strong></a> (88 images).</p>
                    <p><a href="https://s3-us-west-2.amazonaws.com/seed-the-change/seed-the-change-banners-2017.zip"
                          target="_blank" rel="noopener"><img
                                src="https://s3-us-west-2.amazonaws.com/seed-the-change/STC-Partner-Banners-White.png"
                                alt="STC Banners & Images Example"
                                style="width:100%; max-width: 100%; height: auto;"></a></p>
                    <p>The above images are available in various sizes and styles to meet all your online needs.</p>
                    <p>They're available in 5 different styles (White, Teal, Black, Image Version A, and Image Version
                        B).</p>
                    <p>If you'd like to use any of these images in print please <a
                            href="mailto:support@seedthechange.org">contact us via email</a> and we'll send you the
                        print friendly vector versions.</p>
                    <p><a href="https://s3-us-west-2.amazonaws.com/seed-the-change/seed-the-change-banners-2017.zip"
                          target="_blank" rel="noopener">Download Now (3.9MB)</a></p>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h2 class="clearfix">Copy &amp; Paste Content</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="postbox">
                        <h2 class="hndle">Program Info Page
                            <small>- use the following as a basis for explaining your partnership with us</small>
                        </h2>
                        <div class="inside">
                            <div class="copy-paste">
                                <h3><?php echo get_bloginfo('name'); ?> Tree Planting Program</h3>
                                <p>At <?php echo get_bloginfo('name'); ?> we're proud to be partners with Seed The
                                    Change and doing our part to make the world a better place.</p>
                                <p>Deforestation is the leading cause of climate change.</p>
                                <p>If we want to protect the future of our planet, and our own health, tree planting has
                                    to be a part of the plan.</p>
                                <p>We're committed to rebuilding and restoring the environment by planting one tree with
                                    every product sold online.</p>
                                <p>The world loses millions of acres of trees due to deforestation every year. In just a
                                    year, one tree has the ability to produce 120 kilograms of oxygen and absorb as much
                                    carbon as a car produces driving 41,000 kms.</p>
                                <p>Every product you buy from us will plant one tree, allowing you to personally impact
                                    the live of those less fortunate.</p>
                                <p>For more information explore the <a href="https://seedthechange.org" target="_blank"
                                                                       rel="noopener">www.SeedTheChange.org</a> website.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="postbox">
                        <h2 class="hndle">Facts &amp; Snippets
                            <small>- use any or all of the following throughout your site, marketing materials, and
                                social media.
                            </small>
                        </h2>
                        <div class="inside">
                            <div class="copy-paste">
                                <p>Planting just one tree and growing it for 30 years will offset the emissions from a
                                    drive of about 500 kms, or a flight of about 2,000 kms.</p>
                                <p>1 tree makes on average 16.67 reams of copy paper or 8,333 sheets.</p>
                                <p>One and a half acres of forest is cut down every second.</p>
                                <p>A single mature tree can release enough oxygen back into the atmosphere to support 2
                                    human beings.</p>
                                <p>Each tree provides an estimated $7 savings in annual environmental benefits,
                                    including energy conservation and reduced pollution.</p>
                                <p>Just one tree can store 1,250 kilograms of carbon. That's the equivalent of
                                    offsetting a return flight from Los Angeles to London.</p>
                                <p>Just 16 trees can offset the annual carbon emissions of an average western household
                                    over its 30 year life.</p>
                                <p>70% of the worlds plants and animals live in forests and are losing their habitats to
                                    deforestation.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
