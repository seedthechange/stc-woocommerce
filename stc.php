<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://bryceyork.com
 * @since             1.0
 * @package           Stc
 *
 * @wordpress-plugin
 * Plugin Name:       Seed The Change
 * Plugin URI:        http://seedthechange.org
 * Description:       Connect your WooCommerce store to the Seed The Change backend - plant trees, grow profits.
 * Version:           1.2
 * Author:            Bryce York
 * Author URI:        http://bryceyork.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       stc
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-stc-activator.php
 */
function activate_stc() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-stc-activator.php';
	Stc_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-stc-deactivator.php
 */
function deactivate_stc() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-stc-deactivator.php';
	Stc_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_stc' );
register_deactivation_hook( __FILE__, 'deactivate_stc' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-stc.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_stc() {

	$plugin = new Stc();
	$plugin->run();

}
run_stc();
